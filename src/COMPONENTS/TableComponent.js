import React, { Component } from "react";
import Swal from "sweetalert2";

export default class TableComponent extends Component {
  HandleClickShow = (item) => {
    Swal.fire({
      title: "Your Information",
      html: `<h1  class="text-black font-bold text-xl first-letter:capitalize"> id: ${item.id} <br>Username: ${item.userName} <br>Email: ${item.email} <br>Age: ${item.age}</h1>`,
      confirmButtonColor: "#3085d6",
      confirmButtonText: "OK",
    });
  };

  render() {
    return (
      <div>
        <div class="relative m-auto w-full pt-10 py-16 overflow-x-auto sm:overflow-x-auto md:overflow-hidden">
          <table class="text-sm w-5/6 m-auto text-center text-black ">
            <thead class="uppercase">
              <tr>
                <th scope="col" class="px-6 py-3">
                  id
                </th>
                <th scope="col" class="px-6 py-3">
                  email
                </th>
                <th scope="col" class="px-6 py-3">
                  username
                </th>
                <th scope="col" class="px-6 py-3">
                  age
                </th>
                <th scope="col" class="px-6 py-3">
                  action
                </th>
              </tr>
            </thead>
            <tbody>
            {this.props.data?.map((item) => (
                <tr class="odd:bg-white even:bg-pink-300 ">
                  <td
                    scope="row"
                    class="px-6 py-4 font-medium text-black whitespace-nowrap dark:text-white"
                  >
                    {item.id}
                  </td>
                  <td class="px-6 py-4">
                  {item.email}
                  </td>
                  <td class="px-6 py-4">
                    {item.userName}
                  </td>
                  <td class="px-6 py-4">
                  {item.age}
                  </td>
                  <td class="px-6 py-4 block ">
                    <button
                      onClick={() => this.props.HandleClickStatus(item.id)}
                      type="button"
                      class={`first-letter:uppercase focus:outline-none text-white  focus:ring-4 font-medium rounded-lg text-sm  py-2.5 mr-2 mb-2 w-40 ${
                        item.status == "pending"
                          ? " bg-red-600"
                          : "bg-green-700"
                      } `}
                    >
                      {item.status}
                    </button>
                    <button
                      onClick={() => this.HandleClickShow(item)}
                      type="button"
                      class="first-letter:uppercase text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm  py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 w-40"
                    >
                      show more
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
