import React from "react";

class Button extends React.Component {
    constructor() {
      super();
      this.state = {
        count: 0,
      };}

    updateCount() {
      this.setState((prevState, props) => {
        return { count: prevState.count + 1}
      });
    }

    render() {
      return (<button className="bg-blue-800 text-white p-5 mt-5 rounded-lg" onClick={() => this.updateCount()}>
                Clicked {this.state.count} times
              </button>);}
  }
export default Button; 