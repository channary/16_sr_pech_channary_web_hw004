import React, { Component } from 'react'
import TableComponent from './TableComponent';


export default class InputComponent extends Component {

    constructor() {
        super();
        this.state = {
            student: [
                // {id : 1, email: "pechnary15@gmail.com", userName : "Johnny", age: "23", status: "pending"},
            ],
            newStuGmail: "",
            newStuName: "",
            newStuAge: "",
            newStuStatus: "pending",
            isEmailValidate: false,
            isNameValidate: false,
            isAgeValidate: false,
            nameError: "",
            emailError: "",
            ageError: ""
        };
    }

    handleChangeGmail = (event) => {
        this.setState({
            newStuGmail: event.target.value
        });
    }
    handleChangeName = (event) => {
        this.setState({
            newStuName: event.target.value
        });
    }

    handleChangeAge = (event) => {
        this.setState({
            newStuAge: event.target.value
        });
    }

    HandleClickStatus = (clickId) => {
        this.state.student.map((item) =>
            item.id == clickId ? (item.status = item.status == "pending" ? "done" : "pending") : item.id
        );

        this.setState({
            student: this.state.student
        })
    }

    submitForm = (e) => {
        e.preventDefault();
        var newStuName = this.state.newStuName;
        var newStuGmail = this.state.newStuGmail;
        var newStuAge = this.state.newStuAge;

        var isNameValidate = true;
        var isEmailValidate = true;
        var isAgeValidate = true;

        var emailError = "";
        var nameError = "";
        var ageError = "";

        let pattEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        let pattName = /[^a-z]/gi;
        let pattAge = /^[1-9][0-9]*/;


        if (newStuGmail === "") {
            emailError = "Gmail can't be empty."
            isEmailValidate = false
        } else if (!(newStuGmail.match(pattEmail) != null)) {
            emailError = "Invalid Gmail"
            isEmailValidate = false
        } else {
            emailError = ""
            isEmailValidate = true
        }


        if (newStuName === "") {
            nameError = "User Name can't be empty."
            isNameValidate = false
        } else if (newStuName.match(pattName) != null) {
            nameError = "Invalid Name"
            isNameValidate = false
        } else {
            nameError = ""
            isNameValidate = true
        }

        if (newStuAge === "") {
            ageError = "Age can't be empty."
            isAgeValidate = false
        } else if (!(newStuAge.match(pattAge) != null)) {
            ageError = "Invalid Age"
            isAgeValidate = false
        } else {
            ageError = ""
            isAgeValidate = true
        }


        if (isAgeValidate && isEmailValidate && isNameValidate) {
            const newObj = {
                id: this.state.student.length + 1, 
                email: this.state.newStuGmail,
                userName: this.state.newStuName, 
                age: this.state.newStuAge,
                status: this.state.newStuStatus,
            }
            console.log(newObj);
            this.setState({
                student: [...this.state.student, newObj],
                nameError: "",
                emailError: "",
                ageError: ""
            })
        } else {
            this.setState({
                nameError, emailError, ageError
            })
        }

        

    }


    render() {

        return (
            <div class="bg-green-200 bg-gradient-to-b from-indigo-500 via-purple-500 to-pink-500">
                <div class=" m-auto w-full text-lg  ">
                    <div class=" text-4xl font-bold text-center first-letter:capitalize py-10">
                        <span className='bg-clip-text text-transparent bg-gradient-to-r from-blue-600 to-pink-500'>please fill your </span>
                        <span>information</span>
                    </div>

                    {/* Form User Input */}
                    <form class="w-5/6 m-auto" onSubmit={this.submitForm}>

                        <div class="py-4">
                            <label for="website-admin" class="block mb-2 font-medium text-gray-900 dark:text-white">Your Email</label>
                            <div className='text-red-500 text-sm'>{this.state.emailError}</div>
                            <div class="flex">
                                <span class="inline-flex items-center px-3  text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                                    📩
                                </span>
                                <input onChange={this.handleChangeGmail} type="text" id="website-admin" class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full  p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="bonnieGreen@gmail.com" ></input>
                            </div>
                        </div>

                        <div class="py-4">
                            <label for="website-admin" class="block mb-2  font-medium text-gray-900 dark:text-white">Username</label>
                            <div className='text-red-500 text-sm'>{this.state.nameError}</div>
                            <div class="flex">
                                <span class="inline-flex items-center px-3  text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                                    🐱‍👤
                                </span>
                                <input onChange={this.handleChangeName} type="text" id="website-admin" class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Bonnie Green"></input>
                            </div>
                        </div>
                        <div class="py-4">
                            <label for="website-admin" class="block mb-2 font-medium text-gray-900 dark:text-white">Age</label>
                            <div className='text-red-500 text-sm'>{this.state.ageError}</div>
                            <div class="flex">
                                <span class="inline-flex items-center px-3 text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                                    💔
                                </span>
                                <input onChange={this.handleChangeAge} type="text" id="website-admin" class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="23"></input>
                            </div>
                        </div>
                        {/* Button Submit */}
                        <div class="text-center">
                            <button type='submit' class="inline-flex items-center justify-center p-0.5 overflow-hidden font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-purple-600 to-blue-500 group-hover:from-purple-600 group-hover:to-blue-500 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800">
                                <span class="px-10 py-2.5 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0">
                                    Register
                                </span>
                            </button>
                        </div>

                    </form>
                </div>
                <TableComponent data={this.state.student} HandleClickStatus={this.HandleClickStatus} />
            </div>
        )
    }
}
